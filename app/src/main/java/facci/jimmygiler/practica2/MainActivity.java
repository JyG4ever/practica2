package facci.jimmygiler.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button Login;
    Button Guardar;
    Button Buscar;
    Button PasarParametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Login = findViewById(R.id.btnLogin);
        Guardar = findViewById(R.id.btnGuardar);
        Buscar = findViewById(R.id.btnBuscar);
        PasarParametro = findViewById(R.id.btnpParametro);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent guardar = new Intent(MainActivity.this, GuardarActivity.class);
                startActivity(guardar);
            }
        });
        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buscar = new Intent(MainActivity.this, BuscarActivity.class);
                startActivity(buscar);
            }
        });
        PasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pasarparametro = new Intent(MainActivity.this, PasarParametroActivity.class);
                startActivity(pasarparametro);
            }
        });

    }
}
