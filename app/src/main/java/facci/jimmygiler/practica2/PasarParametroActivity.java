package facci.jimmygiler.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametroActivity extends AppCompatActivity {
    EditText cajaDatos;
    Button botonEnviar;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        cajaDatos = findViewById(R.id.txtpParametro);
        botonEnviar = findViewById(R.id.btnpParametro);
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametroActivity.this, recibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }
}
