package facci.jimmygiler.practica2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class recibirParametro extends AppCompatActivity {
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);
        txt = findViewById(R.id.txt1);
        Bundle bundle = this.getIntent().getExtras();
        txt.setText(bundle.getString("dato"));
    }
}
